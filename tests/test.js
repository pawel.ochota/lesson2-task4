const compare = require('..');

describe('compare', () => {
  it('should returns a number', async () => {
    const result = compare(1, 1);

    expect(typeof result).toBe('number');
  });

  it('should returns 1 when first argument is bigger than the second one', async () => {
    expect(compare(2, 1)).toBe(1);
    expect(compare(-10, -11)).toBe(1);
    expect(compare(-9999, -10000)).toBe(1);
    expect(compare(10001, 10000)).toBe(1);
  });

  it('should returns -1 when first argument is smaller than the second one', async () => {
    expect(compare(1, 2)).toBe(-1);
    expect(compare(-10, 0)).toBe(-1);
    expect(compare(-100000, -9999)).toBe(-1);
    expect(compare(10000, 10001)).toBe(-1);
  });

  it('should returns 0 when arguments are equal', async () => {
    expect(compare(1, 1)).toBe(0);
    expect(compare(-10, -10)).toBe(0);
    expect(compare(-9999, -9999)).toBe(0);
    expect(compare(10000, 10000)).toBe(0);
  });
});
